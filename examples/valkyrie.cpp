#include<iostream>
#include<vector>
#include<string>
#include<algorithm>
#include<regex>

#include"../include/avl.hpp"



using listing_t = TypeAvl::Avl<std::string, std::vector<std::string>>;
auto make_valkyrie_listing()->listing_t;

auto get_command(std::string)->std::pair<std::string, std::string>;

auto show_help()->void;
auto do_list(const listing_t&)->void;
auto do_show(const listing_t&, std::string)->void;
auto do_count(const listing_t&, std::string)->void;



auto main(int argc, char* argv[]) -> int
{
	show_help();

	// Avl does not allow copy, but allows moving
	auto listing = make_valkyrie_listing();

	while (true)
	{
		std::string input;
		getline(std::cin, input);
		auto command = get_command(input);

		if (command.first == "list")
			do_list(listing);
		else if (command.first == "show")
			do_show(listing, command.second);
		else if (command.first == "count")
			do_count(listing, command.second);
		else if (command.first == "quit")
			return 0;
		else
			std::cout << "Unknown command." << std::endl;
	}
}



auto show_help()->void
{
	std::cout << "Example project for TypeAvl. Commands are lowercase. ";
	std::cout << "Type help to show this text again. This program was not ";
	std::cout << "made with usability in mind. Be a warrior! " << std::endl;
	std::cout << "Commands:" << std::endl;
	std::cout << "\t list: shows all registered Valkyries" << std::endl;
	std::cout << "\t show (name): shows some places where was referred to as a Valkyrie. Ex: \"show eir\"" << std::endl;
	std::cout << "\t count (regex): count names that satisfy regex. Ex: \"count ra.*r\"" << std::endl;
	std::cout << "\t quit: exits this epic piece of software." << std::endl;
}



auto make_valkyrie_listing()->listing_t
{
	listing_t listing;

	// Extracted from wikipedia and then simplified a bit

	// Inserting with `insert` is possible
	listing.insert(listing_t::value_type("brynhildr", std::vector<std::string>{ "skaldskaparmal" }));

	// Same behavior as std::map::insert for return type
	auto insert_eir_result = listing.insert(listing_t::value_type("eir", std::vector<std::string>()));
	(*insert_eir_result.first).second.emplace_back("nafnathulur");

	// But using operator[] is more convenient
	listing["geirahod"] = { "grimnismal" };
	listing["geiravor"] = { "nafnathulur" };
	listing["geirdriful"] = { "nafnathulur" };
	listing["geironul"] = { "grimnismal", "nafnathulur" };
	listing["geirskogul"] = { "hakonarmal", "voluspa", "nafnathulur" };
	listing["goll"] = { "grimnismal", "nafnathulur" };
	listing["gondul"] = { "voluspa", "nafnathulur" };
	listing["gudr"] = { "voluspa", "darradarljod", "gylfaginning", "nafnathulur" };
	listing["herfjotur"] = { "grimnismal", "nafnathulur" };
	listing["herja"] = { "nafnathulur" };
	listing["hladgudr"] = { "volundarkvida" };
	listing["hildr"] = { "voluspa", "grimnismal", "darradarljod", "nafnathulur" };
	listing["hjalmthrimul"] = { "nafnathulur" };
	listing["hervor"] = { "volundarkvida" };
	listing["hjorthrimul"] = { "darradarljod", "nafnathulur" };
	listing["hlokk"] = { "grimnismal", "nafnathulur" };
	listing["hrist"] = { "grimnismal", "nafnathulur" };
	listing["hrund"] = { "nafnathulur" };
	listing["kara"] = { "helgakvida hundingsbana ii" };
	listing["mist"] = { "grimnismal", "nafnathulur" };
	listing["olrun"] = { "volundarkvida" };
	listing["randgridr"] = { "grimnismal", "nafnathulur" };
	listing["radgridr"] = { "grimnismal", "nafnathulur" };
	listing["reginleif"] = { "grimnismal", "nafnathulur" };
	listing["rota"] = { "gylfaginning" };
	listing["sanngridr"] = { "darradarljod" };
	listing["sigrdrifa"] = { "sigrdrifumal" };
	listing["sigrun"] = { "helgakvida hundingsbana i", "helgakvida hundingsbana ii" };
	listing["skalmold"] = { "nafnathulur" };
	listing["skeggold"] = { "grimnismal", "nafnathulur" };
	listing["skogul"] = { "hakonarmal", "voluspa", "grimnismal", "nafnathulur" };
	listing["skuld"] = { "voluspa", "gylfaginning", "nafnathulur" };
	listing["sveid"] = { "nafnathulur" };
	listing["svipul"] = { "darradarljod", "nafnathulur" };
	listing["thogn"] = { "nafnathulur" };
	listing["thrima"] = { "nafnathulur" };
	listing["thrudr"] = { "grimnismal", "nafnathulur" };

	return listing;
}



auto get_command(std::string input)->std::pair<std::string, std::string>
{
	std::string command;
	std::string parameter;
	bool is_command = true;

	for (const auto& c : input)
	{
		if (is_command)
		{
			if (c != ' ')
			{
				command += c;
			}
			else
			{
				is_command = false;
			}
		}
		else
		{
			parameter += c;
		}
	}

	return std::pair<std::string, std::string>(command, parameter);
}



auto do_list(const listing_t& listing)->void
{
	std::cout << "Listing all registered names:" << std::endl;

	for (const auto& kv : listing)
	{
		std::cout << "\t- " << kv.first << std::endl;
	}
}



auto do_show(const listing_t& listing, std::string name)->void
{
	auto find_it = listing.find(name);

	if (find_it == std::end(listing))
	{
		std::cout << "Name not found." << std::endl;
	}
	else
	{
		std::cout << "Appears in the following references:" << std::endl;
		for (const auto& reference : (*find_it).second)
		{
			std::cout << "\t- " << reference << std::endl;
		}
	}
}



auto do_count(const listing_t& listing, std::string regex)->void
{
	auto expression = std::regex(regex);

	auto count = std::count_if(
		std::cbegin(listing),
		std::cend(listing),
		[&](const auto& kv) {
			return std::regex_match(kv.first, expression);
		}
	);

	std::cout << "Found " << count << std::endl;
}