#pragma once

#include<memory>
#include<functional> // std::less
#include<type_traits>
#include<stdexcept>

// Special thanks:
//
// - To Arthur O'Dwyer, which "Pitfalls and decision points in implementing const_iterator"
//   helped a lot in understanding how to implement const and non-const iterators.

namespace TypeAvl
{
	// Inner types have been moved out of the Avl class to make its
	// declaration "more expressive" (read as "easier to look at"). This breaks
	// encapsulation and exposes some of the winner workings. This can be
	// used to one's advantage, but there's no guaranteed back-compatibility.

	template <typename ValueType>
	struct Node
	{
		ValueType kv;

		int balance;
		Node* parent;
		std::unique_ptr<Node> left_child;
		std::unique_ptr<Node> right_child;

		// FIXME : by receiving a KeyT and ValT, it might be possible to avoid copy/move
		//         constructors? I.e emplacement operations. Is there a way of emplacing both
		//         values of a pair at once? Or, at least, emplace the pair construction in here.
		Node(Node* parent, const ValueType& kv) :
			parent(parent), kv(kv), balance(0)
		{
		}
	};



	template <bool IsConst, typename ValueType>
	struct Iterator
	{
		using iterator_category = std::bidirectional_iterator_tag;
		using value_type = ValueType;
		using difference_type = std::ptrdiff_t;
		using reference = typename std::conditional_t< IsConst, const value_type&, value_type& >;
		using pointer = typename std::conditional_t< IsConst, const value_type*, value_type* >;

		Iterator(const Iterator&) = default;
		Iterator(Iterator&&) = default;
		Iterator(Node<ValueType>* ref, const std::unique_ptr<Node<ValueType>>* root);

		template<bool WasConst, class = std::enable_if_t<IsConst || !WasConst>>
		Iterator(const Iterator<WasConst, value_type>& rhs) : m_ref(rhs.m_ref), root(rhs.root)
		{
		}


		auto operator++()->Iterator&;
		auto operator++(int)->Iterator&;
		auto operator--()->Iterator&;
		auto operator--(int)->Iterator&;
		auto operator==(const Iterator& rhs) const->bool;
		auto operator!=(const Iterator& rhs) const->bool;
		auto operator*() const->reference;

		auto operator=(const Iterator&)->Iterator & = default;
		auto operator=(Iterator&&)->Iterator & = default;

		// FIXME : rename to ref
		Node<ValueType>* m_ref;

		// When using decrement operator, this allows the node to find the root if
		// it was equivalent to end().
		const std::unique_ptr<Node<ValueType>>* root;

	private:
		friend struct Iterator<!IsConst, ValueType>;
	};



	template <typename KeyT, typename ValT, typename Compare = std::less<KeyT>>
	class Avl
	{
	public:
		using value_type = std::pair<const KeyT, ValT>;
		using node_t = Node<value_type>;
		using size_type = std::size_t;

		using iterator = Iterator<false, value_type>;
		using const_iterator = Iterator<true, value_type>;



		Avl(Compare key_compare = Compare());
		Avl(const Avl&) = delete;
		Avl(Avl&&) = default;
		~Avl() = default;

		auto operator=(const Avl&)->Avl & = delete;
		auto operator=(Avl&&)->Avl & = default;

		////////////////////
		// Element Access //
		////////////////////

		// Returns the value associated with the specified key. If the key was not found,
		// will throw std::out_of_range exception.
		auto at(const KeyT&)->ValT&;
		auto at(const KeyT&) const->const ValT&;

		// Returns a reference to the value associated with the given key. If the key was
		// not present, it is inserted with the default constructor of the value.
		auto operator[](const KeyT& key)->ValT&;

		///////////////
		// Iterators //
		///////////////

		auto begin()->iterator;
		auto end()->iterator;

		auto begin() const->const_iterator;
		auto end() const->const_iterator;

		auto cbegin() const->const_iterator;
		auto cend() const->const_iterator;

		//////////////
		// Capacity //
		//////////////

		// Returns true if this tree has no registered elements
		auto empty() const noexcept->bool;

		// Returns the number of registered elements in the tree
		auto size() const noexcept->size_type;

		///////////////
		// Modifiers //
		///////////////

		// Removes all elements from the tree. All iterators and pointers or references
		// to it's elements values will become invalid.
		auto clear() noexcept->void;

		// Inserts the key-value pair into the tree. Returns an iterator
		// to the newly added element and true on success. If the key was
		// already present, will return an iterator to the existing element
		// and false. In this second case, the value registered with the key
		// won't be changed.
		auto insert(const value_type&)->std::pair<iterator, bool>;

		// Removes the element referenced by the given iterator, and return an iterator
		// pointing to the next element or end() if none. The given iterator must
		// be valid and dereferenceable.
		auto erase(const_iterator)->iterator;
		auto erase(iterator)->iterator;

		// Removes all elements within the range defined by the given @first and
		// @last iterators. Range must be valid and both iteratons must belong to this
		// tree. The deleted range is [first, last). Returns the iterator in the next
		// position after the highest removed element; should be the same as @last.
		auto erase(const_iterator first, const_iterator last)->iterator;

		// Removes the element with the given key, if any. Returns the number
		// of removed elements - should be either 0 or 1.
		auto erase(const KeyT&)->size_type;

		// TODO : implement this insert variant
		//void insert(std::initializer_list<value_type> ilist);

		////////////
		// Lookup //
		////////////

		// Returns how many times the given key is found within the AVL.
		// Will always be either 0 or 1.
		auto count(const KeyT&) const noexcept->size_type;

		// Returns an iterator for the given key, if present. Returns
		// equivalent to end() otherwise.
		auto find(const KeyT&)->iterator;
		auto find(const KeyT&) const->const_iterator;

		// Returns true if the given key is found withing the AVL,
		// or false otherwise.
		auto contains(const KeyT&) const noexcept->bool;

		/////////////////////////////////////////////////
		// Temporary (from initial AVL implementation) //
		/////////////////////////////////////////////////

		auto Del(const KeyT key)->bool;

		/////////////////////////////////////////////////////////
		// Helping functions to aid with testing and debugging //
		// These are supposed to change with no warnings. Dont //
		// use them.                                           //
		/////////////////////////////////////////////////////////

		// Note : now that Node and Iterator have been extracted out of the Avl class,
		//        it might make sense move these functions to the test project only.

		// Returns true if all nodes are considered correct or false otherwise.
		// This function was implemented to aid with debugging issues.
		auto validate_nodes_integrity() const -> bool;

		// Helper function to aid with validating the tree nodes balance
		void ValidateBalance(std::function<void(int, int, int)>) const;



	private:
		Compare m_key_compare;

		size_type m_elements;
		std::unique_ptr<node_t> m_root;

		static inline auto rotate_left_insert(std::unique_ptr<Avl::node_t>&)->void;
		static inline auto rotate_right_insert(std::unique_ptr<Avl::node_t>&)->void;
		static inline auto rotate_left_delete(std::unique_ptr<Avl::node_t>&)->void;
		static inline auto rotate_right_delete(std::unique_ptr<Avl::node_t>&)->void;
		static inline auto rotate_left_right(std::unique_ptr<Avl::node_t>&)->void;
		static inline auto rotate_right_left(std::unique_ptr<Avl::node_t>&)->void;

		// TODO : need a visitor pattern for these use cases
		auto validate_nodes_integrity_recursive(node_t* current, node_t* lastParent) const -> bool;
		auto ValidateBalanceRecursive(const node_t* node, std::function<void(int, int, int)> fn) const->int;

#if ((defined(_MSVC_LANG) && _MSVC_LANG >= 201703L) || __cplusplus >= 201703L)
		static_assert(std::is_convertible_v<const_iterator, const_iterator>);
		static_assert(std::is_convertible_v<iterator, const_iterator>);
		static_assert(!std::is_convertible_v<const_iterator, iterator>);
		static_assert(std::is_convertible_v<iterator, iterator>);

		static_assert(std::is_trivially_copy_constructible_v<const_iterator>);
		static_assert(std::is_trivially_copy_constructible_v<iterator>);
#endif

		////////////////////////////////////////
		// Helper functions to aid with reuse //
		// These might be deleted in favor of //
		// "forced inlining"                  //
		////////////////////////////////////////
		auto erase_from_noderef(std::unique_ptr<node_t>&)->void;
	};



	template <typename KeyT, typename ValT, typename Compare>
	Avl<KeyT, ValT, Compare>::Avl(Compare key_compare) :
		m_key_compare(key_compare), m_elements(0)
	{
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::find(const KeyT& key)->iterator
	{
		auto it = m_root.get();
		while (it != nullptr)
		{
			if (m_key_compare(key, it->kv.first))
			{
				it = it->left_child.get();
			}
			else if (m_key_compare(it->kv.first, key))
			{
				it = it->right_child.get();
			}
			else
			{
				return iterator{ it, &m_root };
			}
		}

		return iterator{ nullptr, &m_root };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::find(const KeyT& key) const->const_iterator
	{
		auto it = m_root.get();
		while (it != nullptr)
		{
			if (m_key_compare(key, it->kv.first))
			{
				it = it->left_child.get();
			}
			else if (m_key_compare(it->kv.first, key))
			{
				it = it->right_child.get();
			}
			else
			{
				return const_iterator{ it, &m_root };
			}
		}

		return const_iterator{ nullptr, &m_root };
	}



	// Simple search implementation to avoid building an iterator (not that
	// it should have any penalty). Used on simple query functions such as
	// `count` or `contains`
	template <typename KeyT, typename NodeT, typename Compare>
	inline auto find_node(Compare& key_compare, NodeT* root, const KeyT key)->NodeT*
	{
		auto node = root;

		while (node != nullptr)
		{
			if (key_compare(key, node->kv.first))
			{
				node = node->left_child.get();
			}
			else if (key_compare(node->kv.first, key))
			{
				node = node->right_child.get();
			}
			else
			{
				break;
			}
		}

		return node;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::at(const KeyT& key) -> ValT&
	{
		auto node = find_node(m_key_compare, m_root.get(), key);

		if (node == nullptr)
		{
			throw std::out_of_range{ "key was not found" };
		}

		return node->kv.second;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::at(const KeyT& key) const -> const ValT&
	{
		auto node = find_node(m_key_compare, m_root.get(), key);

		if (node == nullptr)
		{
			throw std::out_of_range{ "key was not found" };
		}

		return node->kv.second;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::operator[](const KeyT& key)->ValT&
	{
		// FIXME : this can be optimized by separating search from insert (avoids
		//         unneeded ValT default constructor if key already exists)
		auto insert_result = insert(value_type(key, ValT()));
		return (*insert_result.first).second;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::count(const KeyT& key) const noexcept-> size_type
	{
		const auto node = find_node(m_key_compare, m_root.get(), key);
		return (node != nullptr ? 1 : 0);
	}

	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::contains(const KeyT& key) const noexcept-> bool
	{
		const auto node = find_node(m_key_compare, m_root.get(), key);
		return (node != nullptr);
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::rotate_left_insert(std::unique_ptr<Avl::node_t>& node)->void
	{
		auto pivot = std::move(node->right_child);
		if (pivot->left_child != nullptr)
		{
			node->right_child = std::move(pivot->left_child);
			node->right_child->parent = node.get();
		}

		pivot.swap(node);
		node->parent = pivot->parent;
		node->left_child = std::move(pivot);
		node->left_child->parent = node.get();

		node->balance = 0;
		node->left_child->balance = 0;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::rotate_right_insert(std::unique_ptr<Avl::node_t>& node)->void
	{
		auto pivot = std::move(node->left_child);
		if (pivot->right_child != nullptr)
		{
			node->left_child = std::move(pivot->right_child);
			node->left_child->parent = node.get();
		}

		pivot.swap(node);
		node->parent = pivot->parent;
		node->right_child = std::move(pivot);
		node->right_child->parent = node.get();

		node->balance = 0;
		node->right_child->balance = 0;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::rotate_left_delete(std::unique_ptr<Avl::node_t>& node)->void
	{
		auto pivot = std::move(node->right_child);
		if (pivot->left_child != nullptr)
		{
			node->right_child = std::move(pivot->left_child);
			node->right_child->parent = node.get();
		}

		pivot.swap(node);
		node->parent = pivot->parent;
		node->left_child = std::move(pivot);
		node->left_child->parent = node.get();

		if (node->balance == 0)
		{
			node->balance = -1;
			node->left_child->balance = 1;
		}
		else
		{
			node->balance = 0;
			node->left_child->balance = 0;
		}
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::rotate_right_delete(std::unique_ptr<Avl::node_t>& node)->void
	{
		auto pivot = std::move(node->left_child);
		if (pivot->right_child != nullptr)
		{
			node->left_child = std::move(pivot->right_child);
			node->left_child->parent = node.get();
		}

		pivot.swap(node);
		node->parent = pivot->parent;
		node->right_child = std::move(pivot);
		node->right_child->parent = node.get();

		if (node->balance == 0)
		{
			node->balance = 1;
			node->right_child->balance = -1;
		}
		else
		{
			node->balance = 0;
			node->right_child->balance = 0;
		}
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::rotate_left_right(std::unique_ptr<Avl::node_t>& node)->void
	{
		auto pivot = std::move(node->left_child->right_child);
		pivot->parent = node->parent;

		if (pivot->left_child != nullptr)
		{
			node->left_child->right_child = std::move(pivot->left_child);
			node->left_child->right_child->parent = node->left_child.get();
		}

		pivot->left_child = std::move(node->left_child);
		pivot->left_child->parent = pivot.get();

		if (pivot->right_child != nullptr)
		{
			node->left_child = std::move(pivot->right_child);
			node->left_child->parent = node.get();
		}

		pivot->right_child = std::move(node);
		pivot->right_child->parent = pivot.get();
		node = std::move(pivot);

		if (node->balance == 0)
		{
			node->left_child->balance = 0;
			node->right_child->balance = 0;
		}
		else if (node->balance > 0)
		{
			node->balance = 0;
			node->left_child->balance = -1;
			node->right_child->balance = 0;
		}
		else
		{
			node->balance = 0;
			node->left_child->balance = 0;
			node->right_child->balance = 1;
		}
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::rotate_right_left(std::unique_ptr<Avl::node_t>& node)->void
	{
		auto pivot = std::move(node->right_child->left_child);
		pivot->parent = node->parent;

		if (pivot->right_child != nullptr)
		{
			node->right_child->left_child = std::move(pivot->right_child);
			node->right_child->left_child->parent = node->right_child.get();
		}

		pivot->right_child = std::move(node->right_child);
		pivot->right_child->parent = pivot.get();

		if (pivot->left_child != nullptr)
		{
			node->right_child = std::move(pivot->left_child);
			node->right_child->parent = node.get();
		}

		pivot->left_child = std::move(node);
		pivot->left_child->parent = pivot.get();
		node = std::move(pivot);

		if (node->balance == 0)
		{
			node->left_child->balance = 0;
			node->right_child->balance = 0;
		}
		else if (node->balance > 0)
		{
			node->balance = 0;
			node->left_child->balance = -1;
			node->right_child->balance = 0;
		}
		else
		{
			node->balance = 0;
			node->left_child->balance = 0;
			node->right_child->balance = 1;
		}
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::erase_from_noderef(std::unique_ptr<node_t>& current_node)->void
	{
		--m_elements;

		node_t* rebalance_current = nullptr;
		if (current_node->left_child == nullptr)
		{
			if (current_node->right_child == nullptr)
			{
				// Removing a leaf node
				if (current_node == m_root)
				{
					m_root.reset();
					return;
				}
				else
				{
					if (current_node->parent->left_child == current_node)
					{
						current_node->parent->balance += 1;
					}
					else
					{
						current_node->parent->balance -= 1;
					}
					rebalance_current = current_node->parent;
					current_node.reset();
				}
			}
			else
			{
				// Removing a node with only a right child. Since this node does
				// not have a left child, the right child is a leaf, or the current node
				// would not be in AVL shape otherwise.
				current_node->right_child->parent = current_node->parent;
				current_node = std::move(current_node->right_child);

				rebalance_current = current_node.get();
			}
		}
		else
		{
			if (current_node->right_child == nullptr)
			{
				// Removing a node with only a left child. Since this node does
				// not have a right child, the left child is a leaf, or the current node
				// would not be in AVL shape otherwise.
				current_node->left_child->parent = current_node->parent;
				current_node = std::move(current_node->left_child);

				rebalance_current = current_node.get();
			}
			else
			{
				auto min_node = &current_node->right_child;
				if ((*min_node)->left_child == nullptr)
				{
					// Corner case where we can quickly remove the node
					(*min_node)->balance = current_node->balance - 1;
					(*min_node)->left_child = std::move(current_node->left_child);
					(*min_node)->left_child->parent = min_node->get();
					(*min_node)->parent = current_node->parent;
					current_node = std::move((*min_node));

					rebalance_current = current_node.get();
				}
				else
				{
					auto min_node_parent = min_node;
					while ((*min_node)->left_child != nullptr)
					{
						min_node_parent = min_node;
						min_node = &(*min_node)->left_child;
					}

					rebalance_current = min_node_parent->get();

					auto swap_tmp = std::move(*min_node);
					(*min_node_parent)->left_child = std::move(swap_tmp->right_child);
					if ((*min_node_parent)->left_child != nullptr)
					{
						(*min_node_parent)->left_child->parent = min_node_parent->get();
					}
					(*min_node_parent)->balance += 1;

					swap_tmp->left_child = std::move(current_node->left_child);
					swap_tmp->left_child->parent = swap_tmp.get();
					swap_tmp->right_child = std::move(current_node->right_child);
					swap_tmp->right_child->parent = swap_tmp.get();
					swap_tmp->balance = current_node->balance;
					swap_tmp->parent = current_node->parent;
					current_node = std::move(swap_tmp);
				}
			}
		}



		node_t* rebalance_previous;

		while (rebalance_current != nullptr)
		{
			if (rebalance_current->balance == -2)
			{
				std::unique_ptr<node_t>* rotation_point;
				if (rebalance_current->parent == nullptr)
				{
					rotation_point = &m_root;
				}
				else
				{
					rotation_point = (rebalance_current->parent->left_child.get() == rebalance_current) ?
						&rebalance_current->parent->left_child : &rebalance_current->parent->right_child;
				}

				if (rebalance_current->left_child->balance <= 0)
				{
					rotate_right_delete(*rotation_point);
				}
				else
				{
					rotate_left_right(*rotation_point);
				}

				rebalance_current = rotation_point->get();
			}

			if (rebalance_current->balance == 2)
			{
				std::unique_ptr<node_t>* rotation_point;
				if (rebalance_current->parent == nullptr)
				{
					rotation_point = &m_root;
				}
				else
				{
					rotation_point = (rebalance_current->parent->left_child.get() == rebalance_current) ?
						&rebalance_current->parent->left_child : &rebalance_current->parent->right_child;
				}

				if (rebalance_current->right_child->balance >= 0)
				{
					rotate_left_delete(*rotation_point);
				}
				else
				{
					rotate_right_left(*rotation_point);
				}

				rebalance_current = rotation_point->get();
			}

			if (rebalance_current->balance != 0)
			{
				// If parent is unbalanced, it means we didn't change the height of the
				// tree. There's no more propagation upwards
				break;
			}

			if (rebalance_current->parent != nullptr)
			{
				rebalance_previous = rebalance_current;
				rebalance_current = rebalance_current->parent;
				if (rebalance_current->left_child.get() == rebalance_previous)
				{
					rebalance_current->balance += 1;
				}
				else
				{
					rebalance_current->balance -= 1;
				}
			}
			else
			{
				break;
			}
		}
	}



	// Iterators
	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::begin() ->iterator
	{
		if (m_root == nullptr)
			return iterator{ nullptr,&m_root };

		auto it = m_root.get();
		while (it->left_child != nullptr)
		{
			it = it->left_child.get();
		}

		return iterator(it, &m_root);
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::end() ->iterator
	{
		return iterator{ nullptr, &m_root };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::begin() const ->const_iterator
	{
		if (m_root == nullptr)
			return const_iterator{ nullptr, &m_root };

		auto it = m_root.get();
		while (it->left_child != nullptr)
		{
			it = it->left_child.get();
		}

		return const_iterator{ it, &m_root };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::end() const->const_iterator
	{
		return const_iterator{ nullptr, &m_root };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::cbegin() const->const_iterator
	{
		return const_iterator{ begin() };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::cend() const->const_iterator
	{
		return const_iterator{ end() };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::empty() const noexcept->bool
	{
		return (m_elements == 0);
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::size() const noexcept->size_type
	{
		return m_elements;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::clear() noexcept->void
	{
		m_root.reset();
		m_elements = 0;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::insert(const value_type& kv)->std::pair<iterator, bool>
	{
		if (m_root == nullptr)
		{
			m_root = std::make_unique<node_t>(nullptr, kv);
			++m_elements;

			return std::pair<iterator, bool>(iterator{ m_root.get(), &m_root }, true);
		}


		node_t* last = m_root.get();

		while (true)
		{
			if (m_key_compare(kv.first, last->kv.first))
			{
				if (last->left_child == nullptr)
				{
					last->left_child = std::make_unique<node_t>(last, kv);
					last = last->left_child.get();
					break;
				}
				else
				{
					last = last->left_child.get();
				}
			}
			else if (m_key_compare(last->kv.first, kv.first))
			{
				if (last->right_child == nullptr)
				{
					last->right_child = std::make_unique<node_t>(last, kv);
					last = last->right_child.get();
					break;
				}
				else
				{
					last = last->right_child.get();
				}
			}
			else
			{
				return std::pair<iterator, bool>(iterator{ last , &m_root }, false);;
			}
		}


		++m_elements;

		const auto inserted = last;
		auto current = last->parent;

		while (current != nullptr)
		{
			if (current->left_child.get() == last)
			{
				current->balance--;
			}
			else
			{
				current->balance++;
			}

			if (current->balance == 0)
			{
				// Adding this element actually rebalanced a node
				break;
			}

			if (current->balance == -2)
			{
				std::unique_ptr<node_t>* rotation_point;
				if (current->parent == nullptr)
				{
					rotation_point = &m_root;
				}
				else
				{
					rotation_point = (current->parent->left_child.get() == current) ?
						&current->parent->left_child : &current->parent->right_child;
				}

				if (last->balance <= 0)
				{
					rotate_right_insert(*rotation_point);
				}
				else
				{
					rotate_left_right(*rotation_point);
				}

				break;
			}

			if (current->balance == 2)
			{
				std::unique_ptr<node_t>* rotation_point;
				if (current->parent == nullptr)
				{
					rotation_point = &m_root;
				}
				else
				{
					rotation_point = (current->parent->right_child.get() == current) ?
						&current->parent->right_child : &current->parent->left_child;
				}

				if (last->balance >= 0)
				{
					rotate_left_insert(*rotation_point);
				}
				else
				{
					rotate_right_left(*rotation_point);
				}

				break;
			}

			last = current;
			current = current->parent;
		}

		return std::pair<iterator, bool>(iterator{ inserted, &m_root }, true);
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::erase(const_iterator pos)->iterator
	{
		std::unique_ptr<node_t>* current_node;

		if (pos.m_ref->parent == nullptr)
		{
			current_node = &m_root;
		}
		else
		{
			current_node = (pos.m_ref->parent->right_child.get() == pos.m_ref) ?
				&pos.m_ref->parent->right_child : &pos.m_ref->parent->left_child;
		}

		++pos;
		erase_from_noderef(*current_node);

		return iterator{ pos.m_ref, &m_root };
	}




	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::erase(iterator pos)->iterator
	{
		return erase(const_iterator{ pos });
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::erase(const_iterator first, const_iterator last)->iterator
	{
		// Note : not exactly the most performatic, but will do for now.
		// FIXME : search if there is an algorithm to mass-remove elements from AVL, and if
		//         it can be implemented within this tree.
		while (first != last)
		{
			first = erase(first);
		}

		return iterator{ first.m_ref, &m_root };
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::erase(const KeyT& key)->size_type
	{
		auto find_it = find(key);

		if (find_it != end())
		{
			erase(find_it);
			return 1;
		}
		else
		{
			return 0;
		}
	}



	template <bool IsConst, typename ValueType>
	Iterator<IsConst, ValueType>::Iterator(Node<ValueType>* ref, const std::unique_ptr<Node<ValueType>>* root) :
		m_ref(ref), root(root)
	{
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator++()->Iterator&
	{
		if (m_ref->right_child != nullptr)
		{
			m_ref = m_ref->right_child.get();
			while (m_ref->left_child != nullptr)
			{
				m_ref = m_ref->left_child.get();
			}
		}
		else
		{
			while (m_ref->parent != nullptr && m_ref == m_ref->parent->right_child.get())
			{
				m_ref = m_ref->parent;
			}

			m_ref = m_ref->parent;
		}

		return *this;
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator++(int)->Iterator&
	{
		return ++(*this);
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator--()->Iterator&
	{
		if (m_ref != nullptr)
		{
			if (m_ref->left_child != nullptr)
			{
				m_ref = m_ref->left_child.get();
				while (m_ref->right_child != nullptr)
				{
					m_ref = m_ref->right_child.get();
				}
			}
			else
			{
				while (m_ref->parent != nullptr && m_ref->parent->right_child.get() != m_ref)
				{
					m_ref = m_ref->parent;
				}
				m_ref = m_ref->parent;
			}
		}
		else
		{
			m_ref = root->get();
			while (m_ref->right_child != nullptr)
			{
				m_ref = m_ref->right_child.get();
			}
		}

		return *this;
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator--(int)->Iterator&
	{
		return --(*this);
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator==(const Iterator& rhs) const->bool
	{
		return (this->m_ref == rhs.m_ref);
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator!=(const Iterator& rhs) const->bool
	{
		return (this->m_ref != rhs.m_ref);
	}



	template <bool IsConst, typename ValueType>
	auto Iterator<IsConst, ValueType>::operator*() const->reference
	{
		return m_ref->kv;
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::validate_nodes_integrity_recursive(node_t* current, node_t* lastParent) const -> bool
	{
		if (current == nullptr)
			return true;

		bool thisOk = current->parent == lastParent;
		bool leftChildOk = validate_nodes_integrity_recursive(current->left_child.get(), current);
		bool rightChildOk = validate_nodes_integrity_recursive(current->right_child.get(), current);

		// Implemented this way so that it is easy to put breakpoints when debugging
		if (thisOk && leftChildOk && rightChildOk)
		{
			return true;
		}
		else
		{
			return false;
		}
	}



	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::validate_nodes_integrity() const -> bool
	{
		return validate_nodes_integrity_recursive(m_root.get(), nullptr);
	}



	// Validation visitor. Need to think about this again
	template <typename KeyT, typename ValT, typename Compare>
	auto Avl<KeyT, ValT, Compare>::ValidateBalanceRecursive(
		const Avl<KeyT, ValT, Compare>::node_t* node,
		std::function<void(int, int, int)> fn
	) const -> int
	{
		if (node == nullptr)
			return 0;

		auto leftHeight = ValidateBalanceRecursive(node->left_child.get(), fn);
		auto rightHeight = ValidateBalanceRecursive(node->right_child.get(), fn);
		fn(node->balance, leftHeight, rightHeight);

		return (leftHeight > rightHeight) ? leftHeight + 1 : rightHeight + 1;
	}



	template <typename KeyT, typename ValT, typename Compare>
	void Avl<KeyT, ValT, Compare>::ValidateBalance(std::function<void(int, int, int)> fn) const
	{
		ValidateBalanceRecursive(m_root.get(), fn);
	}
}