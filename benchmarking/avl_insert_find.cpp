#include<iostream>
#include<vector>

#include"../include/avl.hpp"
#include"bench.hpp"



auto main(int argc, char* argv[]) -> int
{
	TypeAvl::Avl<int, int> avl{};
	bench_insert_and_find(avl, 1 << 20);

	return 0;
}