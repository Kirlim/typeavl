#include<iostream>
#include<vector>
#include<chrono>
#include<unordered_map>

#include"bench.hpp"



auto main(int argc, char* argv[]) -> int
{
	std::unordered_map<int, int> unordered_map{};
	bench_insert_and_find(unordered_map, 1 << 20);

	return 0;
}
