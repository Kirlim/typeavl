#include<iostream>
#include<vector>
#include<map>

#include"bench.hpp"



auto main(int argc, char* argv[]) -> int
{
	std::map<int, int> map{};
	bench_iterator(map, 1 << 20);

	return 0;
}
