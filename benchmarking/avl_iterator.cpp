#include<iostream>
#include<vector>
#include<numeric>
#include<random>
#include<chrono>

#include"../include/avl.hpp"
#include"bench.hpp"



auto main(int argc, char* argv[]) -> int
{
	TypeAvl::Avl<int, int> avl{};
	bench_iterator(avl, 1 << 20);

	return 0;
}
