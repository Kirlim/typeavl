#pragma once

#include<algorithm>
#include<iostream>
#include<vector>
#include<numeric>
#include<random>
#include<chrono>

#include<functional>



// Simple helper function to run basic benchmarking.
// BENCHMARKING IS HARD
// This function can easily be a bad bench method.

// TODO : function that runs OP without needing to sort elements over
auto bench(
	int elements_count,
	std::function<void(const std::vector<int>&)> insert_op,
	std::function<void(const std::vector<int>&)> execute_op
)->void
{
	std::vector<int> keys(elements_count);
	std::iota(std::begin(keys), std::end(keys), 0);

	auto seed = std::chrono::system_clock::now().time_since_epoch().count();

	std::shuffle(std::begin(keys), std::end(keys), std::default_random_engine((unsigned int)(seed)));
	auto insert_op_start = std::chrono::system_clock::now();
	insert_op(keys);
	auto insert_op_end = std::chrono::system_clock::now();
	std::chrono::duration<double> insert_op_diff = insert_op_end - insert_op_start;

	std::shuffle(std::begin(keys), std::end(keys), std::default_random_engine((unsigned int)(seed)));
	auto execute_op_start = std::chrono::system_clock::now();
	execute_op(keys);
	auto execute_op_end = std::chrono::system_clock::now();
	std::chrono::duration<double> execute_op_diff = execute_op_end - execute_op_start;

	std::cout << "Run results: " << std::endl;
	std::cout << "\tInsert Operation:\t" << insert_op_diff.count() << "s" << std::endl;
	std::cout << "\tExecute Operation:\t" << execute_op_diff.count() << "s" << std::endl;
	std::cout << std::endl;
}

auto bench(
	int elements_count,
	std::function<void(const std::vector<int>&)> insert_op,
	std::function<void()> execute_op
)->void
{
	std::vector<int> keys(elements_count);
	std::iota(std::begin(keys), std::end(keys), 0);

	auto seed = std::chrono::system_clock::now().time_since_epoch().count();

	std::shuffle(std::begin(keys), std::end(keys), std::default_random_engine((unsigned int)(seed)));
	auto insert_op_start = std::chrono::system_clock::now();
	insert_op(keys);
	auto insert_op_end = std::chrono::system_clock::now();
	std::chrono::duration<double> insert_op_diff = insert_op_end - insert_op_start;

	auto execute_op_start = std::chrono::system_clock::now();
	execute_op();
	auto execute_op_end = std::chrono::system_clock::now();
	std::chrono::duration<double> execute_op_diff = execute_op_end - execute_op_start;

	std::cout << "Run results: " << std::endl;
	std::cout << "\tInsert Operation:\t" << insert_op_diff.count() << "s" << std::endl;
	std::cout << "\tExecute Operation:\t" << execute_op_diff.count() << "s" << std::endl;
	std::cout << std::endl;
}



// Does insertions with `insert` method and retrieves elements
// with `find` method.
template <typename Container>
auto bench_insert_and_find(Container& container, int elements_count)
{
	bench(
		elements_count,
		[&container](const std::vector<int>& keys) {
			for (auto key : keys) container.insert(typename Container::value_type(key, key));
		},
		[&container](const std::vector<int>& keys) {
			int sum = 0;
			for (auto key : keys)
			{
				sum += (*container.find(key)).second;
			}
			std::cout << "Sum: " << sum << std::endl;
		}
		);
}

// Does insertions with `insert` method and then iterates through
// all elements with a ranged-for loop.
template <typename Container>
auto bench_iterator(Container& container, int elements_count)
{
	bench(
		elements_count,
		[&container](const std::vector<int>& keys) {
			for (auto key : keys) container.insert(typename Container::value_type(key, key));
		},
		[&container]() {
			int sum = 0;
			for (auto element : container)
			{
				sum += element.second;
			}
			std::cout << "Sum: " << sum << std::endl;
		}
		);
}

