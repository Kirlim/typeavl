[TOC]

# About

TypeAvl is a **c++14 or above** implementation of the **AVL tree** data structure. It's interface is
intended to be **similar to std::map**. Due to my inexperience with library code and even lack of
understanding of all restriction upon the STL, **full compatibility to std::map behavior is not promised
nor guaranteed**.

Currently, the characteristics of this implementation are:

- Use of `std::unique_ptr` to manage nodes (kinda nice, but sometimes I regret this choice);
- Nodes have a raw pointer to the parent.
- Nodes (un)balance are calculated per insertion or deletion operation. Only affected nodes should be verified.
- No custom allocators support yet - I need to understand this before I can implement it into the tree...
- No thread safety controls. A thread-safe AVL, if it even does make sense (another data structure could
  be abetter choice), should be treated as an entirely different algorithm, in my opinion.

TypeAvl started is a simple project with the intention of learning both the AVL Tree data structure
and how to write library-like code. In reality, it made clear how hard it is to make STL-like code,
even more with an acceptable performance. If you're experienced and have suggestions on how to improve
functinality, performance or code-quality, I'd be glad to hear your feedback!

Currently, the code is far from being library-like, and most of `std::map` functions are not implemented.
Also, there's no strong, educated en-masse testing to "guarantee" correct behavior in all tricky corner cases.

## Examples

Basic `std::map` operations should work with the same syntax. For example, inserting elements:

```c++
using listing_t = TypeAvl::Avl<std::string, std::vector<std::string>>;

auto make_valkyrie_listing()->listing_t
{
	listing_t listing;

	// Inserting with `insert` is possible
	listing.insert(std::pair("brynhildr", std::vector<std::string>{ "skaldskaparmal" }));

	// Same behavior as std::map::insert for return type
	auto insert_eir_result = listing.insert(std::pair("eir", std::vector<std::string>()));
	(*insert_eir_result.first).second.emplace_back("nafnathulur");

	// But using operator[] is more convenient
	listing["geirahod"] = { "grimnismal" };
	listing["geiravor"] = { "nafnathulur" };
	listing["geirdriful"] = { "nafnathulur" };

	/* (...) */

	// There's no copy between trees, so this is a move
	return listing;
}
```

Basic iterators are implemented. For now, they are forward iterators and there's no reverse iterator yet.
Day-to-day iterators work should behave as expected. As with `std::map`, iterators returns a `std::pair`.
For example, using ranged-for loops:

```c++
auto do_list(const listing_t& listing)->void
{
	// Keys are ordered. If you don't need to ordered keys, you
	// shouldn't need an AVL, or even an std::map :)
	std::cout << "Listing all registered names:" << std::endl;

	for (const auto& kv : listing)
	{
		std::cout << "\t- " << kv.first << std::endl;
	}
}
```

Of course, this means that algorithms based on iterators should work as well. Example using `algorithm`:

```c++
auto do_count(const listing_t& listing, std::string regex)->void
{
	auto expression = std::regex(regex);

	auto count = std::count_if(
		std::cbegin(listing),
		std::cend(listing),
		[&](const auto& kv) {
			return std::regex_match(kv.first, expression);
		}
	);

	std::cout << "Found " << count << std::endl;
}
```

# Thread Safety

There are no thread-safety controls.

 - Concurrent insertions and/or deletions may corrupt the tree.
 - Concurrent insertion/deletion while executing reads or using iterators may corrupt the read or the iterator.
 - Concurrent reads/iterators are "read only" and will not affect the tree in any way.
 - Insertions should not invalidate any iterators if they weren't changed during an insert operation.
 - Deletions should not invalidate any iterators if they weren't changed during a delete operation, and the
   iterator wasn't pointing to the deleted node.
 - The tree does not copy the nodes or move them from memory address after they are created. This means
   that values should be safe to concurrent reads. Changing elements values concurrently won't affect the
   tree, but may be a race condition when changing them.

## Will there be a thread safe version?

After this AVL implementation becomes acceptable, I'd like to make a thread-safe version. However,
I have no knowledge on how to do so, and adivices are welcome. My first thought is inspired on a
CPP Con 20 presentation, "A Multi-threaded, Transaction-Based Locking Strategy for Containers", by
Bob Steagall ([youtube link](https://www.youtube.com/watch?v=Pi243hGxDyA)). However, I feel that
trees are a different beast.

# Wishlist

Be mostly compatible with `std::map`, the following functions are expected. Some may be removed in the future if it makes more sense in not implementing them.

Methods may be marked as implemented, but not all expected overloads may be present. Refer to the class declaration to check which variations have been implemented.

Construction:

- [ ] Constructors

- [ ] operator=

Iterators:

- [X] make iterators bidirectional

- [X] begin

- [X] end

- [ ] rbegin

- [ ] rend

- [X] cbegin 

- [X] cend 

- [ ] crbegin 

- [ ] crend 

Capacity:

- [X] empty

- [X] size

- [ ] max_size

Element access:

- [X] operator[] (partial)

- [X] at 

Modifiers:

- [X] insert (partial)

- [X] erase

- [ ] swap

- [X] clear

- [ ] emplace 

- [ ] emplace_hint 

Observers:

- [ ] key_comp

- [ ] value_comp

Operations:

- [X] find

- [X] count (partial)

- [X] contains (partial)

- [ ] lower_bound

- [ ] upper_bound

- [ ] equal_range

Allocator

- [ ] get_allocator

# Performance

No strong performance comparisons have been made yet. The benchmarking codes could have some work, too.

