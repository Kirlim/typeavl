#include"catch.hpp"

#include<numeric>

#include"../include/avl.hpp"
#include"validation.hpp"

using namespace TypeAvl;



TEST_CASE("Empty tree", "[Avl]")
{
	Avl<int, int> avl{};

	verifyGetValueFail(avl, 1);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 0);
	REQUIRE(avl.validate_nodes_integrity());
}



TEST_CASE("Adding first element", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, 1, 100);
	verifyGetValue(avl, 1, 100);

	verifyGetValueFail(avl, 2);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 1);
	REQUIRE(avl.validate_nodes_integrity());
}



TEST_CASE("Adding root and left child", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, 1, 100);
	insert_and_check(avl, -1, -100);

	verifyGetValue(avl, 1, 100);
	verifyGetValue(avl, -1, -100);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 2);
	REQUIRE(avl.validate_nodes_integrity());
}



TEST_CASE("Adding root and right child", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, 1, 100);
	insert_and_check(avl, 2, 200);

	verifyGetValue(avl, 1, 100);
	verifyGetValue(avl, 2, 200);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 2);
	REQUIRE(avl.validate_nodes_integrity());
}



TEST_CASE("Adding three decreasing elements to cause right rotation", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, 0, 0);
	insert_and_check(avl, -1, -100);
	insert_and_check(avl, -2, -200);

	verifyGetValue(avl, 0, 0);
	verifyGetValue(avl, -1, -100);
	verifyGetValue(avl, -2, -200);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 3);
	REQUIRE(avl.validate_nodes_integrity());
}



TEST_CASE("Adding three increasing elements to cause left rotation", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, 0, 0);
	insert_and_check(avl, 1, 100);
	insert_and_check(avl, 2, 200);

	verifyGetValue(avl, 0, 0);
	verifyGetValue(avl, 1, 100);
	verifyGetValue(avl, 2, 200);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 3);
	REQUIRE(avl.validate_nodes_integrity());
}



TEST_CASE("Adding three elements to cause a left-right rotation", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, 1, 100);
	insert_and_check(avl, -1, -100);
	insert_and_check(avl, 0, 0);

	verifyGetValue(avl, 1, 100);
	verifyGetValue(avl, -1, -100);
	verifyGetValue(avl, 0, 0);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 3);
	REQUIRE(avl.validate_nodes_integrity());
}

TEST_CASE("Adding three elements to cause a right-left rotation", "[Avl]")
{
	Avl<int, int> avl{};

	insert_and_check(avl, -1, -100);
	insert_and_check(avl, 1, 100);
	insert_and_check(avl, 0, 0);

	verifyGetValue(avl, -1, -100);
	verifyGetValue(avl, 1, 100);
	verifyGetValue(avl, 0, 0);

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 3);
	REQUIRE(avl.validate_nodes_integrity());
}

TEST_CASE("Additional rotations tests (on short avl)", "[Avl]")
{
	// Test cases posted by Griffin on
	// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
	Avl<int, int> shortAvl;

	insert_and_check(shortAvl, 20, 20);
	insert_and_check(shortAvl, 4, 4);
	insert_and_check(shortAvl, 26, 26);
	insert_and_check(shortAvl, 3, 3);
	insert_and_check(shortAvl, 9, 9);

	SECTION("Insert 15")
	{
		insert_and_check(shortAvl, 15, 15);

		verifyGetValue(shortAvl, 3, 3);
		verifyGetValue(shortAvl, 4, 4);
		verifyGetValue(shortAvl, 9, 9);
		verifyGetValue(shortAvl, 15, 15);
		verifyGetValue(shortAvl, 20, 20);
		verifyGetValue(shortAvl, 26, 26);

		shortAvl.ValidateBalance(validateBalance);
		validate_avl_size(shortAvl, 6);
		REQUIRE(shortAvl.validate_nodes_integrity());
	}

	SECTION("Insert 8")
	{
		insert_and_check(shortAvl, 8, 8);

		verifyGetValue(shortAvl, 3, 3);
		verifyGetValue(shortAvl, 4, 4);
		verifyGetValue(shortAvl, 8, 8);
		verifyGetValue(shortAvl, 9, 9);
		verifyGetValue(shortAvl, 20, 20);
		verifyGetValue(shortAvl, 26, 26);

		shortAvl.ValidateBalance(validateBalance);
		validate_avl_size(shortAvl, 6);
		REQUIRE(shortAvl.validate_nodes_integrity());
	}
}

TEST_CASE("Additional rotations tests (on long avl)", "[Avl]")
{
	// Test cases posted by Griffin on
	// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
	Avl<int, int> longAvl;

	insert_and_check(longAvl, 20, 20);
	insert_and_check(longAvl, 4, 4);
	insert_and_check(longAvl, 26, 26);
	insert_and_check(longAvl, 3, 3);
	insert_and_check(longAvl, 9, 9);
	insert_and_check(longAvl, 21, 21);
	insert_and_check(longAvl, 30, 30);
	insert_and_check(longAvl, 2, 2);
	insert_and_check(longAvl, 7, 7);
	insert_and_check(longAvl, 11, 11);

	SECTION("Insert 15")
	{
		insert_and_check(longAvl, 15, 15);

		verifyGetValue(longAvl, 2, 2);
		verifyGetValue(longAvl, 3, 3);
		verifyGetValue(longAvl, 4, 4);
		verifyGetValue(longAvl, 7, 7);
		verifyGetValue(longAvl, 9, 9);
		verifyGetValue(longAvl, 11, 11);
		verifyGetValue(longAvl, 15, 15);
		verifyGetValue(longAvl, 20, 20);
		verifyGetValue(longAvl, 21, 21);
		verifyGetValue(longAvl, 26, 26);
		verifyGetValue(longAvl, 30, 30);

		longAvl.ValidateBalance(validateBalance);
		validate_avl_size(longAvl, 11);
		REQUIRE(longAvl.validate_nodes_integrity());
	}

	SECTION("Insert 8")
	{
		insert_and_check(longAvl, 8, 8);

		verifyGetValue(longAvl, 2, 2);
		verifyGetValue(longAvl, 3, 3);
		verifyGetValue(longAvl, 4, 4);
		verifyGetValue(longAvl, 7, 7);
		verifyGetValue(longAvl, 8, 8);
		verifyGetValue(longAvl, 9, 9);
		verifyGetValue(longAvl, 11, 11);
		verifyGetValue(longAvl, 20, 20);
		verifyGetValue(longAvl, 21, 21);
		verifyGetValue(longAvl, 26, 26);
		verifyGetValue(longAvl, 30, 30);

		longAvl.ValidateBalance(validateBalance);
		validate_avl_size(longAvl, 11);
		REQUIRE(longAvl.validate_nodes_integrity());
	}
}

TEST_CASE("Additional rotations tests (on short avl) - mirrored", "[Avl]")
{
	// Test cases posted by Griffin on
	// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
	//
	// Insertions changed so that the resulting tree and rotations are mirrored
	Avl<int, int> shortAvl;

	insert_and_check(shortAvl, 20, 20);
	insert_and_check(shortAvl, 10, 10);
	insert_and_check(shortAvl, 30, 30);
	insert_and_check(shortAvl, 25, 25);
	insert_and_check(shortAvl, 35, 35);

	SECTION("Insert 22")
	{
		insert_and_check(shortAvl, 22, 22);

		verifyGetValue(shortAvl, 10, 10);
		verifyGetValue(shortAvl, 20, 20);
		verifyGetValue(shortAvl, 22, 22);
		verifyGetValue(shortAvl, 25, 25);
		verifyGetValue(shortAvl, 30, 30);
		verifyGetValue(shortAvl, 35, 35);

		shortAvl.ValidateBalance(validateBalance);
		validate_avl_size(shortAvl, 6);
		REQUIRE(shortAvl.validate_nodes_integrity());
	}

	SECTION("Insert 27")
	{
		insert_and_check(shortAvl, 27, 27);

		verifyGetValue(shortAvl, 10, 10);
		verifyGetValue(shortAvl, 20, 20);
		verifyGetValue(shortAvl, 25, 25);
		verifyGetValue(shortAvl, 27, 27);
		verifyGetValue(shortAvl, 30, 30);
		verifyGetValue(shortAvl, 35, 35);

		shortAvl.ValidateBalance(validateBalance);
		validate_avl_size(shortAvl, 6);
		REQUIRE(shortAvl.validate_nodes_integrity());
	}
}

TEST_CASE("Additional rotations tests (on long avl) - mirrored", "[Avl]")
{
	// Test cases posted by Griffin on
	// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
	//
	// Insertions changed so that the resulting tree and rotations are mirrored
	Avl<int, int> longAvl;

	insert_and_check(longAvl, 20, 20);
	insert_and_check(longAvl, 10, 10);
	insert_and_check(longAvl, 30, 30);
	insert_and_check(longAvl, 5, 5);
	insert_and_check(longAvl, 15, 15);
	insert_and_check(longAvl, 25, 25);
	insert_and_check(longAvl, 35, 35);
	insert_and_check(longAvl, 22, 22);
	insert_and_check(longAvl, 27, 27);
	insert_and_check(longAvl, 40, 40);

	SECTION("Insert 21")
	{
		insert_and_check(longAvl, 21, 21);

		verifyGetValue(longAvl, 5, 5);
		verifyGetValue(longAvl, 10, 10);
		verifyGetValue(longAvl, 15, 15);
		verifyGetValue(longAvl, 20, 20);
		verifyGetValue(longAvl, 21, 21);
		verifyGetValue(longAvl, 22, 22);
		verifyGetValue(longAvl, 25, 25);
		verifyGetValue(longAvl, 27, 27);
		verifyGetValue(longAvl, 30, 30);
		verifyGetValue(longAvl, 35, 35);
		verifyGetValue(longAvl, 40, 40);

		longAvl.ValidateBalance(validateBalance);
		validate_avl_size(longAvl, 11);
		REQUIRE(longAvl.validate_nodes_integrity());
	}

	SECTION("Insert 26")
	{
		insert_and_check(longAvl, 26, 26);

		verifyGetValue(longAvl, 5, 5);
		verifyGetValue(longAvl, 10, 10);
		verifyGetValue(longAvl, 15, 15);
		verifyGetValue(longAvl, 20, 20);
		verifyGetValue(longAvl, 22, 22);
		verifyGetValue(longAvl, 25, 25);
		verifyGetValue(longAvl, 26, 26);
		verifyGetValue(longAvl, 27, 27);
		verifyGetValue(longAvl, 30, 30);
		verifyGetValue(longAvl, 35, 35);
		verifyGetValue(longAvl, 40, 40);

		longAvl.ValidateBalance(validateBalance);
		validate_avl_size(longAvl, 11);
		REQUIRE(longAvl.validate_nodes_integrity());
	}
}

TEST_CASE("Inserting many elements in increasing and decreasing order", "[Avl]")
{
	Avl<int, int> avl{};

	SECTION("Increasing order")
	{
		for (int i = 0; i < 513; ++i)
		{
			insert_and_check(avl, i, i);
		}
		for (int i = 0; i < 513; ++i)
		{
			verifyGetValue(avl, i, i);
		}

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 513);
		REQUIRE(avl.validate_nodes_integrity());
	}

	SECTION("Decreasing order")
	{
		for (int i = 512; i >= 0; --i)
		{
			insert_and_check(avl, i, i);
		}
		for (int i = 512; i >= 0; --i)
		{
			verifyGetValue(avl, i, i);
		}

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 513);
		REQUIRE(avl.validate_nodes_integrity());
	}
}

TEST_CASE("Inserting elements in random order", "[Avl]")
{
	// This test case is here to aid in finding errors, but since
	// insertion order is random, it's not guaranteed it will find unknown
	// bugs.
	Avl<int, int> avl;

	std::vector<int> elements(2048);
	std::iota(std::begin(elements), std::end(elements), 0);

	auto seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffle(std::begin(elements), std::end(elements), std::default_random_engine((unsigned int)(seed)));

	for (auto i : elements)
	{
		insert_and_check(avl, i, i);
	}

	avl.ValidateBalance(validateBalance);
	validate_avl_size(avl, 2048);
	REQUIRE(avl.validate_nodes_integrity());

	for (auto i : elements)
	{
		verifyGetValue(avl, i, i);
	}
}



TEST_CASE("Operator []", "[Avl]")
{
	Avl<int, int> avl{};

	SECTION("Inserting elements")
	{
		avl[0] = 100;
		avl[1] = 200;
		avl[2] = 300;

		verifyGetValue(avl, 0, 100);
		verifyGetValue(avl, 1, 200);
		verifyGetValue(avl, 2, 300);

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 3);
		REQUIRE(avl.validate_nodes_integrity());
	}

	SECTION("Modifying elements")
	{

		avl[0] = 100;
		avl[1] = 200;
		avl[2] = 300;

		avl[0] *= 2;
		avl[1] *= 2;
		avl[2] *= 2;

		verifyGetValue(avl, 0, 200);
		verifyGetValue(avl, 1, 400);
		verifyGetValue(avl, 2, 600);

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 3);
		REQUIRE(avl.validate_nodes_integrity());
	}
}

