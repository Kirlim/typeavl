#include"validation.hpp"

#include<algorithm>

#include"catch.hpp"


auto insert_and_check(TypeAvl::Avl<int, int>& avl, int key, int value)-> void
{
	auto result = avl.insert(TypeAvl::Avl<int, int>::value_type(key, value));

	INFO("key: " << key << " value: " << value);
	REQUIRE((result.second == true && (*result.first).first == key && (*result.first).second == value));
}



auto verifyGetValueNonConst(TypeAvl::Avl<int, int>& avl, int key, int expectedValue) ->void
{
	auto countKey = avl.count(key);
	bool containsKey = avl.contains(key);

	auto iteratorByFind = avl.find(key);
	auto keyByFind = (*iteratorByFind).first;
	auto valueByFind = (*iteratorByFind).second;
	auto valueByAt = avl.at(key);

	REQUIRE((
		countKey == 1 &&
		containsKey &&

		keyByFind == key &&
		valueByFind == expectedValue &&
		valueByAt == expectedValue
		)
	);
}

auto verifyGetValueConst(const TypeAvl::Avl<int, int>& avl, int key, int expectedValue) ->void
{
	auto countKey = avl.count(key);
	bool containsKey = avl.contains(key);

	auto iteratorByFind = avl.find(key);
	auto keyByFind = (*iteratorByFind).first;
	auto valueByFind = (*iteratorByFind).second;
	auto valueByAt = avl.at(key);

	REQUIRE((
		countKey == 1 &&
		containsKey &&

		keyByFind == key &&
		valueByFind == expectedValue &&
		valueByAt == expectedValue
		)
	);
}

auto verifyGetValue(TypeAvl::Avl<int, int>& avl, int key, int expectedValue) ->void
{
	verifyGetValueNonConst(avl, key, expectedValue);
	verifyGetValueConst(avl, key, expectedValue);
}



auto verifyGetValueFailNonConst(TypeAvl::Avl<int, int>& avl, int key) ->void
{
	auto countKey = avl.count(key);
	bool containsKey = avl.contains(key);

	auto iteratorByFind = avl.find(key);

	REQUIRE((
		countKey == 0 &&
		!containsKey &&

		iteratorByFind == std::end(avl)
		)
	);

	REQUIRE_THROWS_AS(avl.at(key), std::out_of_range);
}

auto verifyGetValueFailConst(const TypeAvl::Avl<int, int>& avl, int key) ->void
{
	auto countKey = avl.count(key);
	bool containsKey = avl.contains(key);

	auto iteratorByFind = avl.find(key);

	REQUIRE((
		countKey == 0 &&
		!containsKey &&

		iteratorByFind == std::end(avl)
		)
	);

	REQUIRE_THROWS_AS(avl.at(key), std::out_of_range);
}

auto verifyGetValueFail(TypeAvl::Avl<int, int>& avl, int key)->void
{
	verifyGetValueFailNonConst(avl, key);
	verifyGetValueFailConst(avl, key);
}



void validateBalance(int nodeBalance, int leftChildBalance, int rightChildBalance)
{
	auto expectedBalance = rightChildBalance - leftChildBalance;
	REQUIRE((expectedBalance >= -1 && expectedBalance <= 1));
	REQUIRE(nodeBalance == expectedBalance);
}



auto validate_avl_size(TypeAvl::Avl<int, int>& avl, int expected_size)->void
{
	auto expected_empty = (expected_size == 0);

	auto empty = avl.empty();
	auto distance = std::distance(std::begin(avl), std::end(avl));
	auto current_size = avl.size();

	INFO("Expected avl size: " << expected_size << " Current: " << current_size << " Distance: " << distance);
	REQUIRE((empty == expected_empty && current_size == expected_size && distance == expected_size));
}
