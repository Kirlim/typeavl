#include"catch.hpp"

#include<numeric>
#include<chrono>

#include"../include/avl.hpp"

using namespace TypeAvl;



TEST_CASE("Iterating over empty tree", "[Avl]")
{
	Avl<int, int> avl{};
	REQUIRE(std::begin(avl) == std::end(avl));
}



TEST_CASE("Iterating over tree with only root", "[Avl]")
{
	Avl<int, int> avl{};
	avl.insert(Avl<int, int>::value_type(0, 0));

	SECTION("Increment")
	{
		int i = 0;
		for (auto it : avl)
		{
			REQUIRE(it.first == i);
			REQUIRE(it.second == i);
			++i;
		}

		REQUIRE(i == 1);
	}

	SECTION("Decrement")
	{
		auto it = std::end(avl);
		auto i = 0;
		while (it != std::begin(avl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -1);
	}
}



TEST_CASE("Iterating over tree with three elements", "[Avl]")
{
	Avl<int, int> avl{};
	avl.insert(Avl<int, int>::value_type(0, 0));
	avl.insert(Avl<int, int>::value_type(-1, -1));
	avl.insert(Avl<int, int>::value_type(1, 1));

	SECTION("Increment")
	{
		int i = -1;
		for (auto it : avl)
		{
			REQUIRE(it.first == i);
			REQUIRE(it.second == i);
			++i;
		}

		REQUIRE(i == 2);
	}

	SECTION("Decrement")
	{
		auto it = std::end(avl);
		auto i = 1;
		while (it != std::begin(avl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -2);
	}
}



TEST_CASE("Iterating over tree with a min element that has a right child", "[Avl]")
{
	Avl<int, int> avl{};
	avl.insert(Avl<int, int>::value_type(0, 0));
	avl.insert(Avl<int, int>::value_type(-2, -2));
	avl.insert(Avl<int, int>::value_type(1, 1));
	avl.insert(Avl<int, int>::value_type(-1, -1));

	SECTION("Increment")
	{
		int i = -2;
		for (auto it : avl)
		{
			REQUIRE(it.first == i);
			REQUIRE(it.second == i);
			++i;
		}

		REQUIRE(i == 2);
	}

	SECTION("Decrement")
	{
		auto it = std::end(avl);
		auto i = 1;
		while (it != std::begin(avl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -3);
	}
}



TEST_CASE("Iterating over a random tree with many elements", "[Avl]")
{
	Avl<int, int> avl;

	std::vector<int> elements(2048);
	std::iota(std::begin(elements), std::end(elements), 0);

	auto seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffle(std::begin(elements), std::end(elements), std::default_random_engine((unsigned int)(seed)));

	for (auto i : elements)
	{
		avl.insert(Avl<int, int>::value_type(i, i));
	}

	SECTION("Increment")
	{
		int i = 0;
		for (auto it : avl)
		{
			REQUIRE(it.first == i);
			REQUIRE(it.second == i);
			++i;
		}

		REQUIRE(i == 2048);
	}

	SECTION("Decrement")
	{
		auto it = std::end(avl);
		auto i = 2047;
		while (it != std::begin(avl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -1);
	}
}



TEST_CASE("Iterating over a random constant tree with many elements", "[Avl]")
{
	Avl<int, int> avl;

	std::vector<int> elements(2048);
	std::iota(std::begin(elements), std::end(elements), 0);

	auto seed = std::chrono::system_clock::now().time_since_epoch().count();
	shuffle(std::begin(elements), std::end(elements), std::default_random_engine((unsigned int)(seed)));

	for (auto i : elements)
	{
		avl.insert(Avl<int, int>::value_type(i, i));
	}

	const auto& constAvl = avl;

	SECTION("Increment")
	{
		int i = 0;
		for (auto it : constAvl)
		{
			REQUIRE(it.first == i);
			REQUIRE(it.second == i);
			++i;
		}

		REQUIRE(i == 2048);
	}

	SECTION("Decrement")
	{
		auto it = std::end(constAvl);
		auto i = 2047;
		while (it != std::begin(constAvl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -1);
	}
}



TEST_CASE("Iterator incrementing/decrementing", "[Avl]")
{
	Avl<int, int> avl{};
	avl.insert(Avl<int, int>::value_type(0, 0));
	avl.insert(Avl<int, int>::value_type(1, 1));

	SECTION("Iterator increment")
	{
		REQUIRE((*avl.begin()++).first == 1);
		REQUIRE((*++avl.begin()).first == 1);
	}

	SECTION("Const Iterator increment")
	{
		REQUIRE((*avl.cbegin()++).first == 1);
		REQUIRE((*++avl.cbegin()).first == 1);
	}

	SECTION("Iterator decrement")
	{
		REQUIRE((*avl.end()--).first == 1);
		REQUIRE((*--avl.end()).first == 1);
	}

	SECTION("Const Iterator decrement")
	{
		REQUIRE((*avl.cend()--).first == 1);
		REQUIRE((*--avl.cend()).first == 1);
	}
}



TEST_CASE("Iterating over tree by using begin() and end() methods (and variations)", "[Avl]")
{
	Avl<int, int> avl{};
	avl.insert(Avl<int, int>::value_type(0, 0));
	avl.insert(Avl<int, int>::value_type(-1, -1));
	avl.insert(Avl<int, int>::value_type(1, 1));

	SECTION("begin() end()")
	{
		int i = -1;
		for (auto it = avl.begin(); it != avl.end(); it++)
		{
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			++i;
		}

		REQUIRE(i == 2);
	}

	SECTION("Decrementing begin() end()")
	{
		auto it = std::end(avl);
		auto i = 1;
		while (it != std::begin(avl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -2);
	}

	SECTION("cbegin() cend()")
	{
		int i = -1;
		for (auto it = avl.cbegin(); it != avl.cend(); it++)
		{
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			++i;
		}

		REQUIRE(i == 2);
	}

	SECTION("Decrementing cbegin() cend()")
	{
		auto it = std::cend(avl);
		auto i = 1;
		while (it != std::cbegin(avl))
		{
			--it;
			REQUIRE((*it).first == i);
			REQUIRE((*it).second == i);
			--i;
		}

		REQUIRE(i == -2);
	}
}



TEST_CASE("Changing element value with iterator", "[Avl]")
{
	Avl<int, int> avl;
	avl.insert(Avl<int, int>::value_type(1, 1));

	auto it = std::begin(avl);
	(*it).second = 100;

	REQUIRE((*avl.find(1)).second == 100);
}
