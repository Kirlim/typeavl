#pragma once

#include"../include/avl.hpp"

auto insert_and_check(TypeAvl::Avl<int, int>& avl, int key, int value)->void;
auto verifyGetValue(TypeAvl::Avl<int, int>& avl, int key, int expectedValue)->void;
auto verifyGetValueFail(TypeAvl::Avl<int, int>& avl, int key)->void;
auto validateBalance(int nodeBalance, int leftChildBalance, int rightChildBalance)->void;
auto validate_avl_size(TypeAvl::Avl<int, int>& avl, int expected_size)->void;

