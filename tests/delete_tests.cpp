#include"catch.hpp"

#include<numeric>
#include<vector>
#include<string>

#include"../include/avl.hpp"
#include"validation.hpp"

using namespace TypeAvl;


// Since there are multiple ways of deleting one node, using a little
// helper so we can test all of them with the same code
struct DeleteOpInfo
{
	std::string delete_description;
	std::function<void(Avl<int, int>&, int)> delete_fail_function;
	std::function<void(Avl<int, int>&, int)> delete_success_function;

	DeleteOpInfo(
		std::string delete_description,
		std::function<void(Avl<int, int>&, int)> delete_fail_function,
		std::function<void(Avl<int, int>&, int)> delete_success_function
	) :
		delete_description(delete_description),
		delete_fail_function(delete_fail_function),
		delete_success_function(delete_success_function)
	{
	}
};

auto make_delete_ops()->std::vector<DeleteOpInfo>
{
	return std::vector<DeleteOpInfo>({
		DeleteOpInfo{
			"Erase from key",
			[&](Avl<int, int>& avl, int key) {REQUIRE(avl.erase(key) == 0); },
			[&](Avl<int, int>& avl,int key) {REQUIRE(avl.erase(key) == 1); }
		},
		DeleteOpInfo{
			"Erase from iterator",
			[&](Avl<int, int>& avl,int key) {/* Pre-condition is that the key is present (pos is dereferenceable) */} ,
			[&](Avl<int, int>& avl,int key) {
				auto it = avl.find(key);
				auto it_inc = it;
				it_inc++;
				REQUIRE(avl.erase(it) == it_inc);
			}
		},
		DeleteOpInfo{
			"Erase from const_iterator",
			[&](Avl<int, int>& avl,int key) {/* Pre-condition is that the key is present (pos is dereferenceable) */} ,
			[&](Avl<int, int>& avl,int key) {
				auto it = const_cast<const Avl<int, int>*>(&avl)->find(key);
				auto it_inc = it;
				it_inc++;
				REQUIRE(Avl<int, int>::const_iterator{ avl.erase(it) } == it_inc);
			}
		},
		DeleteOpInfo {
			"Erase with iterators range",
			[&](Avl<int, int>& avl,int key) {/* Pre-condition is that the range is valid */} ,
			[&](Avl<int, int>& avl,int key) {
				auto it = avl.find(key);
				auto it_inc = it;
				it_inc++;
				REQUIRE(avl.erase(it, it_inc) == it_inc);
			}
		}
	});
}



TEST_CASE("Simple deletions")
{
	auto delete_ops = make_delete_ops();

	for (auto& delete_op : delete_ops)
	{
		Avl<int, int> avl;

		DYNAMIC_SECTION("Deleting from empty tree - " << delete_op.delete_description)
		{
			delete_op.delete_fail_function(avl, 1);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 0);
			REQUIRE(avl.validate_nodes_integrity());
		}

		avl.insert(Avl<int, int>::value_type(0, 0));

		DYNAMIC_SECTION("Deleting non existing from  tree - " << delete_op.delete_description)
		{
			delete_op.delete_fail_function(avl, 10);
			verifyGetValue(avl, 0, 0);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 1);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Deleting root of tree with one element - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 0);
			verifyGetValueFail(avl, 0);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 0);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Adding left element and deleting left element - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-10, -10));
			delete_op.delete_success_function(avl, -10);
			verifyGetValue(avl, 0, 0);
			verifyGetValueFail(avl, -10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 1);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Adding right element and deleting right element - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(10, 10));
			delete_op.delete_success_function(avl, 10);
			verifyGetValue(avl, 0, 0);
			verifyGetValueFail(avl, 10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 1);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Adding left element and deleting root - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-10, -10));
			delete_op.delete_success_function(avl, 0);
			verifyGetValue(avl, -10, -10);
			verifyGetValueFail(avl, 0);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 1);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Adding right element and deleting root - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(10, 10));
			delete_op.delete_success_function(avl, 0);
			verifyGetValue(avl, 10, 10);
			verifyGetValueFail(avl, 0);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 1);
			REQUIRE(avl.validate_nodes_integrity());
		}

		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));

		DYNAMIC_SECTION("Has two childs, remove left child - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, -10);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 10, 10);
			verifyGetValueFail(avl, -10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 2);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Has two childs, remove right child - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 10);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, 0, 0);
			verifyGetValueFail(avl, 10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 2);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Has two childs, remove root - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 0);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, 10, 10);
			verifyGetValueFail(avl, 0);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 2);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting element causing left rotation (parent ends balanced)", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));
		avl.insert(Avl<int, int>::value_type(15, 15));

		DYNAMIC_SECTION(delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, -10);

			verifyGetValueFail(avl, -10);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 3);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting element causing left rotation (parent ends unbalanced)", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));
		avl.insert(Avl<int, int>::value_type(-15, -15));
		avl.insert(Avl<int, int>::value_type(15, 15));
		avl.insert(Avl<int, int>::value_type(3, 3));
		avl.insert(Avl<int, int>::value_type(17, 17));

		DYNAMIC_SECTION(delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, -15);

			verifyGetValueFail(avl, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);
			verifyGetValue(avl, 17, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 6);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}

TEST_CASE("Deleting element causing right rotation (parent ends balanced)", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));
		avl.insert(Avl<int, int>::value_type(-15, -15));

		DYNAMIC_SECTION(delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 10);

			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, 0, 0);
			verifyGetValueFail(avl, 10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 3);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting element causing right rotation (parent ends unbalanced)", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));
		avl.insert(Avl<int, int>::value_type(-15, -15));
		avl.insert(Avl<int, int>::value_type(-5, -5));
		avl.insert(Avl<int, int>::value_type(15, 15));
		avl.insert(Avl<int, int>::value_type(-17, -17));

		DYNAMIC_SECTION(delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 15);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 10, 10);
			verifyGetValueFail(avl, 15);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 6);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting element causing right-left rotation", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));
		avl.insert(Avl<int, int>::value_type(-15, -15));
		avl.insert(Avl<int, int>::value_type(-5, -5));
		avl.insert(Avl<int, int>::value_type(5, 5));
		avl.insert(Avl<int, int>::value_type(15, 15));
		avl.insert(Avl<int, int>::value_type(-17, -17));
		avl.insert(Avl<int, int>::value_type(-3, -3));
		avl.insert(Avl<int, int>::value_type(3, 3));
		avl.insert(Avl<int, int>::value_type(17, 17));

		DYNAMIC_SECTION("Bottom has balance 0 - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-7, -7));

			delete_op.delete_success_function(avl, -17);

			verifyGetValueFail(avl, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -7, -7);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 17, 17);
			verifyGetValue(avl, 15, 15);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 11);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Bottom has balance -1 - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-7, -7));
			avl.insert(Avl<int, int>::value_type(-8, -8));

			delete_op.delete_success_function(avl, -17);

			verifyGetValueFail(avl, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -8, -8);
			verifyGetValue(avl, -7, -7);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 17, 17);
			verifyGetValue(avl, 15, 15);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 12);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Bottom has balance 1 - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-7, -7));
			avl.insert(Avl<int, int>::value_type(-6, -6));

			delete_op.delete_success_function(avl, -17);

			verifyGetValueFail(avl, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -7, -7);
			verifyGetValue(avl, -6, -6);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 17, 17);
			verifyGetValue(avl, 15, 15);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 12);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting element causing left-right rotation", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));
		avl.insert(Avl<int, int>::value_type(-15, -15));
		avl.insert(Avl<int, int>::value_type(-5, -5));
		avl.insert(Avl<int, int>::value_type(5, 5));
		avl.insert(Avl<int, int>::value_type(15, 15));
		avl.insert(Avl<int, int>::value_type(-17, -17));
		avl.insert(Avl<int, int>::value_type(-3, -3));
		avl.insert(Avl<int, int>::value_type(3, 3));
		avl.insert(Avl<int, int>::value_type(17, 17));

		DYNAMIC_SECTION("Bottom has balance 0 - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(7, 7));

			delete_op.delete_success_function(avl, 17);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 7, 7);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);
			verifyGetValueFail(avl, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 11);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Bottom has balance 1 - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(7, 7));
			avl.insert(Avl<int, int>::value_type(8, 8));

			delete_op.delete_success_function(avl, 17);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 7, 7);
			verifyGetValue(avl, 8, 8);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);
			verifyGetValueFail(avl, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 12);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Bottom has balance -1 - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(7, 7));
			avl.insert(Avl<int, int>::value_type(6, 6));

			delete_op.delete_success_function(avl, 17);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 6, 6);
			verifyGetValue(avl, 7, 7);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);
			verifyGetValueFail(avl, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 12);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting element causing double rotation", "[Avl]")
{
	// Test cases posted by Griffin on
	// https://stackoverflow.com/questions/3955680/how-to-check-if-my-avl-tree-implementation-is-correct
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		DYNAMIC_SECTION("As posted - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(5, 5));
			avl.insert(Avl<int, int>::value_type(2, 2));
			avl.insert(Avl<int, int>::value_type(8, 8));
			avl.insert(Avl<int, int>::value_type(1, 1));
			avl.insert(Avl<int, int>::value_type(3, 3));
			avl.insert(Avl<int, int>::value_type(7, 7));
			avl.insert(Avl<int, int>::value_type(10, 10));
			avl.insert(Avl<int, int>::value_type(4, 4));
			avl.insert(Avl<int, int>::value_type(6, 6));
			avl.insert(Avl<int, int>::value_type(9, 9));
			avl.insert(Avl<int, int>::value_type(11, 11));
			avl.insert(Avl<int, int>::value_type(12, 12));

			delete_op.delete_success_function(avl, 1);

			verifyGetValueFail(avl, 1);
			verifyGetValue(avl, 2, 2);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 4, 4);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 6, 6);
			verifyGetValue(avl, 7, 7);
			verifyGetValue(avl, 8, 8);
			verifyGetValue(avl, 9, 9);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 11, 11);
			verifyGetValue(avl, 12, 12);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 11);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Mirrored - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-5, -5));
			avl.insert(Avl<int, int>::value_type(-2, -2));
			avl.insert(Avl<int, int>::value_type(-8, -8));
			avl.insert(Avl<int, int>::value_type(-1, -1));
			avl.insert(Avl<int, int>::value_type(-3, -3));
			avl.insert(Avl<int, int>::value_type(-7, -7));
			avl.insert(Avl<int, int>::value_type(-10, -10));
			avl.insert(Avl<int, int>::value_type(-4, -4));
			avl.insert(Avl<int, int>::value_type(-6, -6));
			avl.insert(Avl<int, int>::value_type(-9, -9));
			avl.insert(Avl<int, int>::value_type(-11, -11));
			avl.insert(Avl<int, int>::value_type(-12, -12));

			delete_op.delete_success_function(avl, -1);

			verifyGetValueFail(avl, -1);
			verifyGetValue(avl, -2, -2);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, -4, -4);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -6, -6);
			verifyGetValue(avl, -7, -7);
			verifyGetValue(avl, -8, -8);
			verifyGetValue(avl, -9, -9);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -11, -11);
			verifyGetValue(avl, -12, -12);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 11);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting elements that have left and right child", "[Avl]")
{
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		avl.insert(Avl<int, int>::value_type(0, 0));
		avl.insert(Avl<int, int>::value_type(-10, -10));
		avl.insert(Avl<int, int>::value_type(10, 10));

		DYNAMIC_SECTION("Remove root in simple tree of height 2 - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 0);

			verifyGetValue(avl, -10, -10);
			verifyGetValueFail(avl, 0);
			verifyGetValue(avl, 10, 10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 2);
			REQUIRE(avl.validate_nodes_integrity());
		}

		avl.insert(Avl<int, int>::value_type(-15, -15));
		avl.insert(Avl<int, int>::value_type(-5, -5));

		DYNAMIC_SECTION("Remove root with right child without children - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 0);

			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValueFail(avl, 0);
			verifyGetValue(avl, 10, 10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 4);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Remove root with right child with another right child - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(15, 15));
			delete_op.delete_success_function(avl, 0);

			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValueFail(avl, 0);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 5);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Remove root with right child with a left child - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(5, 5));
			delete_op.delete_success_function(avl, 0);

			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValueFail(avl, 0);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 10, 10);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 5);
			REQUIRE(avl.validate_nodes_integrity());
		}

		avl.insert(Avl<int, int>::value_type(5, 5));
		avl.insert(Avl<int, int>::value_type(15, 15));
		avl.insert(Avl<int, int>::value_type(-17, -17));
		avl.insert(Avl<int, int>::value_type(-3, -3));
		avl.insert(Avl<int, int>::value_type(3, 3));
		avl.insert(Avl<int, int>::value_type(17, 17));

		DYNAMIC_SECTION("Remove root in more complex tree - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 0);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValueFail(avl, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValue(avl, 10, 10);
			verifyGetValue(avl, 15, 15);
			verifyGetValue(avl, 17, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 10);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Remove mid-tree element with right child that has no left child - " << delete_op.delete_description)
		{
			delete_op.delete_success_function(avl, 10);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValueFail(avl, 10);
			verifyGetValue(avl, 15, 15);
			verifyGetValue(avl, 17, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 10);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Remove mid-tree element with rigth child that has a left child - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(13, 13));

			delete_op.delete_success_function(avl, 10);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValueFail(avl, 10);
			verifyGetValue(avl, 13, 13);
			verifyGetValue(avl, 15, 15);
			verifyGetValue(avl, 17, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 11);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Remove mid-tree element with rigth child that has a left child that has a right child - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(13, 13));
			avl.insert(Avl<int, int>::value_type(14, 14));

			delete_op.delete_success_function(avl, 10);

			verifyGetValue(avl, -17, -17);
			verifyGetValue(avl, -15, -15);
			verifyGetValue(avl, -10, -10);
			verifyGetValue(avl, -5, -5);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 5, 5);
			verifyGetValueFail(avl, 10);
			verifyGetValue(avl, 13, 13);
			verifyGetValue(avl, 14, 14);
			verifyGetValue(avl, 15, 15);
			verifyGetValue(avl, 17, 17);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 12);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Other remotion cases", "[Avl]")
{
	// These test cases have been obtained by some random testing
	// which resultied in errors.
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl;

		DYNAMIC_SECTION("Remove node with right child near the end of the tree - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(2, 2));
			avl.insert(Avl<int, int>::value_type(1, 1));
			avl.insert(Avl<int, int>::value_type(4, 4));
			avl.insert(Avl<int, int>::value_type(0, 0));
			avl.insert(Avl<int, int>::value_type(3, 3));
			avl.insert(Avl<int, int>::value_type(6, 6));
			avl.insert(Avl<int, int>::value_type(5, 5));

			delete_op.delete_success_function(avl, 6);

			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, 1, 1);
			verifyGetValue(avl, 2, 2);
			verifyGetValue(avl, 3, 3);
			verifyGetValue(avl, 4, 4);
			verifyGetValue(avl, 5, 5);
			verifyGetValueFail(avl, 6);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 6);
			REQUIRE(avl.validate_nodes_integrity());
		}

		DYNAMIC_SECTION("Remove node with right child near the end of the tree [mirrored] - " << delete_op.delete_description)
		{
			avl.insert(Avl<int, int>::value_type(-2, -2));
			avl.insert(Avl<int, int>::value_type(-1, -1));
			avl.insert(Avl<int, int>::value_type(-4, -4));
			avl.insert(Avl<int, int>::value_type(0, 0));
			avl.insert(Avl<int, int>::value_type(-3, -3));
			avl.insert(Avl<int, int>::value_type(-6, -6));
			avl.insert(Avl<int, int>::value_type(-5, -5));

			delete_op.delete_success_function(avl, -6);

			verifyGetValue(avl, 0, 0);
			verifyGetValue(avl, -1, -1);
			verifyGetValue(avl, -2, -2);
			verifyGetValue(avl, -3, -3);
			verifyGetValue(avl, -4, -4);
			verifyGetValue(avl, -5, -5);
			verifyGetValueFail(avl, -6);

			avl.ValidateBalance(validateBalance);
			validate_avl_size(avl, 6);
			REQUIRE(avl.validate_nodes_integrity());
		}
	}
}



TEST_CASE("Deleting random element of tree", "[Avl]")
{
	// Simple test case that adds a few layer of elements, and
	// then removes only one element and checks if the tree is
	// still in AVL shape. This is done for each element.
	//
	// This test will cover testing deletion from any tree point
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl{};

		DYNAMIC_SECTION(delete_op.delete_description)
		{
			const int size = 100;
			for (int i = 0; i < size; ++i)
			{
				for (int element = 0; element < size; ++element)
				{
					avl.insert(Avl<int, int>::value_type(element, element));
				}

				delete_op.delete_success_function(avl, i);

				for (int element = 0; element < size; ++element)
				{
					if (i == element)
					{
						verifyGetValueFail(avl, element);
					}
					else
					{
						verifyGetValue(avl, element, element);
					}

					avl.ValidateBalance(validateBalance);
					validate_avl_size(avl, 99);
					REQUIRE(avl.validate_nodes_integrity());
				}
			}
		}
	}
}

TEST_CASE("Deleting random element of tree [mirrored]", "[Avl]")
{
	// Simple test case that adds a few layer of elements, and
	// then removes only one element and checks if the tree is
	// still in AVL shape. This is done for each element.
	//
	// This test will cover testing deletion from any tree point
	auto delete_ops = make_delete_ops();

	for (auto delete_op : delete_ops)
	{
		Avl<int, int> avl{};

		DYNAMIC_SECTION(delete_op.delete_description)
		{
			const int size = 100;
			for (int i = 0; i < size; ++i)
			{
				for (int element = 0; element < size; ++element)
				{
					avl.insert(Avl<int, int>::value_type(-element, -element));
				}

				delete_op.delete_success_function(avl, -i);

				for (int element = 0; element < size; ++element)
				{
					if (i == element)
					{
						verifyGetValueFail(avl, -element);
					}
					else
					{
						verifyGetValue(avl, -element, -element);
					}

					avl.ValidateBalance(validateBalance);
					validate_avl_size(avl, 99);
					REQUIRE(avl.validate_nodes_integrity());
				}
			}
		}
	}
}




TEST_CASE("Deleting range of elements with erase", "[Avl]")
{
	Avl<int, int> avl{};

	SECTION("Deleting all elements from an empty tree")
	{
		auto it = avl.erase(std::begin(avl), std::end(avl));
		REQUIRE(it == std::end(avl));
	}

	avl.insert(Avl<int, int>::value_type(1, 1));
	avl.insert(Avl<int, int>::value_type(2, 2));
	avl.insert(Avl<int, int>::value_type(3, 3));
	avl.insert(Avl<int, int>::value_type(4, 4));
	avl.insert(Avl<int, int>::value_type(5, 5));
	avl.insert(Avl<int, int>::value_type(6, 6));

	SECTION("Deleting first half of elements")
	{
		auto start = avl.find(1);
		auto end = avl.find(4);

		auto it = avl.erase(start, end);
		REQUIRE(it == end);

		verifyGetValueFail(avl, 1);
		verifyGetValueFail(avl, 2);
		verifyGetValueFail(avl, 3);
		verifyGetValue(avl, 4, 4);
		verifyGetValue(avl, 5, 5);
		verifyGetValue(avl, 6, 6);

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 3);
		REQUIRE(avl.validate_nodes_integrity());
	}

	SECTION("Deleting second half of elements")
	{
		auto start = avl.find(4);
		auto end = avl.find(6);

		auto it = avl.erase(start, end++);
		REQUIRE(it == end);

		verifyGetValue(avl, 1, 1);
		verifyGetValue(avl, 2, 2);
		verifyGetValue(avl, 3, 3);
		verifyGetValueFail(avl, 4);
		verifyGetValueFail(avl, 5);
		verifyGetValueFail(avl, 6);

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 3);
		REQUIRE(avl.validate_nodes_integrity());
	}

	SECTION("Deleting all elements")
	{
		auto it = avl.erase(std::begin(avl), std::end(avl));
		REQUIRE(it == std::end(avl));

		verifyGetValueFail(avl, 1);
		verifyGetValueFail(avl, 2);
		verifyGetValueFail(avl, 3);
		verifyGetValueFail(avl, 4);
		verifyGetValueFail(avl, 5);
		verifyGetValueFail(avl, 6);

		avl.ValidateBalance(validateBalance);
		validate_avl_size(avl, 0);
		REQUIRE(avl.validate_nodes_integrity());
	}
}



TEST_CASE("Clearing", "[Avl]")
{
	Avl<int, int> avl;

	SECTION("Clearing an empty tree should not crash")
	{
		avl.clear();
		validate_avl_size(avl, 0);
	}

	SECTION("Adding elements and then clearing the tree")
	{
		avl.insert(Avl<int, int>::value_type(1, 1));
		avl.insert(Avl<int, int>::value_type(2, 2));
		avl.insert(Avl<int, int>::value_type(3, 3));

		validate_avl_size(avl, 3);

		avl.clear();
		validate_avl_size(avl, 0);
	}
}
